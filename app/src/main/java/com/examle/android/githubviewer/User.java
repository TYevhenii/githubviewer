package com.examle.android.githubviewer;

import java.io.Serializable;

class User implements Serializable, ReposCount.Result {
    private static final long serialVersionUID = 1L;

    private int mId;
    private String mName;
    private String mReposCount;
    private String mReposUrl;
    private String mImage;

    public User(int id, String name, String reposUrl, String image) {
        mId = id;
        mName = name;
        mReposUrl = reposUrl;
        mImage = image;
    }

    String getName() {
        return mName;
    }

    String getReposCount() {
        return mReposCount;
    }

    public void setReposCount(String reposCount) {
        mReposCount = reposCount;
    }

    String getImage() {
        return mImage;
    }

    public String getReposUrl() {
        return mReposUrl;
    }

    public int getId() {
        return mId;
    }

    @Override
    public String toString() {
        return "User{" +
                "mId=" + mId +
                ", mName='" + mName + '\'' +
                ", mReposCount='" + mReposCount + '\'' +
                ", mReposUrl='" + mReposUrl + '\'' +
                ", mImage='" + mImage + '\'' +
                '}';
    }


    @Override
    public void onResultReposCount(String count) {
        setReposCount(count);
    }
}
