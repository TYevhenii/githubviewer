package com.examle.android.githubviewer;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RepoListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RepoListFragment extends Fragment implements ReposList.DataAvailability {

    private RecyclerView mRecyclerView;
    private RepoRecyclerViewAdapter mRepoRecyclerViewAdapter;
    private String mTitle;
    private String mUserName;

    public RepoListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment RepoListFragment.
     */
    public static RepoListFragment newInstance(String param1, String param2) {
        RepoListFragment fragment = new RepoListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedViewModel model = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
        model.getSelected().observe(this, item -> {
            mTitle = getActivity().getTitle().toString();
            getActivity().setTitle(mUserName = item.getUser());
            ReposList reposList = new ReposList(this, item.getSelect());
            reposList.execute();
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_repo_list, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.repo_search_list);
        mRepoRecyclerViewAdapter = new RepoRecyclerViewAdapter(new ArrayList<>(), this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(mRepoRecyclerViewAdapter);

        return view;
    }


    @Override
    public void onDataAvailable(List<Repo> users, String baseUri, DownloadStatus status) {
        if(status == DownloadStatus.OK) {
            mRepoRecyclerViewAdapter.loadNewData(users, baseUri);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(mUserName);

    }

    @Override
    public void onPause() {
        getActivity().setTitle(mTitle);
        super.onPause();
    }
}
