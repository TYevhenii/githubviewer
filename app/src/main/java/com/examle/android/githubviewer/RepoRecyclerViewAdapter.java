package com.examle.android.githubviewer;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

class RepoRecyclerViewAdapter extends RecyclerView.Adapter<RepoRecyclerViewAdapter.ReposInfoViewHolder> {
    private static final String TAG = "RepoRecyclerViewAdapter";
    private List<Repo> mRepoList;
    private String mBaseUri;
    private ReposList.DataAvailability mDataAvailability;

    public RepoRecyclerViewAdapter(List<Repo> repoList, ReposList.DataAvailability dataAvailability) {
        mDataAvailability = dataAvailability;
        mRepoList = repoList;
    }

    @Override
    public void onBindViewHolder(ReposInfoViewHolder holder, int position) {
        // Called by the layout manager when it whants new data in an existing row

        if (mRepoList == null || mRepoList.size() == 0) {
            holder.repoName.setText(R.string.empty_repo);
        } else {
            Repo repoItem = mRepoList.get(position);
            Log.d(TAG, "onBindViewHolder: " + repoItem.getRepoName() + " ---> " + position);

            holder.repoName.setText(repoItem.getRepoName());
            holder.repoDescription.setText(repoItem.getRepoDescription());

            if (position + 10 >= mRepoList.size()) {
                ReposList jsonData =
                        new ReposList(mDataAvailability,
                                mBaseUri);
                jsonData.execute();
            }
        }
    }

    @Override
    public ReposInfoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Called by the layout manager when it needs a new view 
        Log.d(TAG, "onCreateViewHolder: new view requested");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.repo_info, parent, false);
        return new ReposInfoViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return ((mRepoList != null) && (mRepoList.size() != 0) ? mRepoList.size() : 1);
    }

    public Repo getRepo(int position) {
        return ((mRepoList != null) && (mRepoList.size() != 0) ? mRepoList.get(position) : null);
    }

    void loadNewData(List<Repo> repos, String baseUri) {
        mRepoList = repos;
        mBaseUri = baseUri;
        notifyDataSetChanged();
    }

    static class ReposInfoViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ReposInfoViewHolder";
        TextView repoName = null;
        TextView repoDescription = null;

        public ReposInfoViewHolder(View itemView) {
            super(itemView);
            Log.d(TAG, "ReposInfoViewHolder: starts");
            this.repoName = (TextView) itemView.findViewById(R.id.repo_name);
            this.repoDescription = (TextView) itemView.findViewById(R.id.repo_description);
        }


    }

}
