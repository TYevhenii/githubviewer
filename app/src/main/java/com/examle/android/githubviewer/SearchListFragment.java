package com.examle.android.githubviewer;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SearchListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchListFragment extends Fragment implements JsonData.DataAvailability, RecyclerItemClickListener.OnRecycylerClickListener {
    private SharedViewModel model;
    private RecyclerView mRecyclerView;
    private SearchRecyclerViewAdapter mSearchRecyclerViewAdapter;

    public SearchListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SearchListFragment.
     */
    public static SearchListFragment newInstance() {
        SearchListFragment fragment = new SearchListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
        model.getSearched().observe(this, item -> {
            JsonData jsonData = new JsonData(SearchListFragment.this, item, "0");
            jsonData.execute();
        });

//        itemSelector.setOnClickListener(item -> {
//            model.select(item);
//        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search_list, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_search_list);
        mSearchRecyclerViewAdapter = new SearchRecyclerViewAdapter(getContext(), new ArrayList<>(), this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(mSearchRecyclerViewAdapter);
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(),
                mRecyclerView, this));
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDataAvailable(List<User> users, String baseUri, DownloadStatus status, Boolean isNeedUpdate) {
        if (status == DownloadStatus.OK) {
            mSearchRecyclerViewAdapter.loadNewData(users, baseUri);
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        SharedViewModel model = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
        User user = mSearchRecyclerViewAdapter.getUser(position);
        if (user != null) {
            UserSelect userSelect = new UserSelect(user.getName(), user.getReposUrl());
            model.select(userSelect);
        }
    }

    @Override
    public void onItemLongClick(View view, int position) {

    }
}
