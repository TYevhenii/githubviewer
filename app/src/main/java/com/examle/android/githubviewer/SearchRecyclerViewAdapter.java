package com.examle.android.githubviewer;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

class SearchRecyclerViewAdapter extends RecyclerView.Adapter<SearchRecyclerViewAdapter.UsersInfoViewHolder> {
    private static final String TAG = "SearchRecyclerViewAdapt";
    private List<User> mUserList;
    private String mBaseUri;
    private Context mContext;
    private JsonData.DataAvailability mDataAvailability;

    public SearchRecyclerViewAdapter(Context context, List<User> userList, JsonData.DataAvailability dataAvailability) {
        mUserList = userList;
        mContext = context;
        mDataAvailability = dataAvailability;
    }

    @Override
    public void onBindViewHolder(UsersInfoViewHolder holder, int position) {
        // Called by the layout manager when it whants new data in an existing row

        if (mUserList == null || mUserList.size() == 0) {
            holder.photo.setImageResource(R.drawable.placeholder);
            holder.name.setText(R.string.empty_user_info);
        } else {
            User userItem = mUserList.get(position);
            Log.d(TAG, "onBindViewHolder: " + userItem.getName() + " ---> " + position);

            Resources resources = mContext.getResources();
            Picasso.get().load(userItem.getImage())
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .resize((int) resources.getDimension(R.dimen.photo_width),
                            (int) resources.getDimension(R.dimen.photo_height))
                    .error(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .into(holder.photo);

            holder.name.setText(userItem.getName());
            holder.reposCount.setText(userItem.getReposCount());
        }
    }

    @Override
    public UsersInfoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Called by the layout manager when it needs a new view 
        Log.d(TAG, "onCreateViewHolder: new view requested");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_info, parent, false);
        return new UsersInfoViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return ((mUserList != null) && (mUserList.size() != 0) ? mUserList.size() : 1);
    }

    public User getUser(int position) {
        return ((mUserList != null) && (mUserList.size() != 0) ? mUserList.get(position) : null);
    }

    void loadNewData(List<User> newUser, String baseUri) {

        if(newUser != null) {
            mUserList.addAll(newUser);
            notifyDataSetChanged();
            mBaseUri = baseUri;
        }
    }

    static class UsersInfoViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "UsersInfoViewHolder";
        ImageView photo = null;
        TextView name = null;
        TextView reposCount = null;

        public UsersInfoViewHolder(View itemView) {
            super(itemView);
            Log.d(TAG, "UsersInfoViewHolder: starts");
            this.photo = (ImageView) itemView.findViewById(R.id.photo);
            this.name = (TextView) itemView.findViewById(R.id.name);
            this.reposCount = (TextView) itemView.findViewById(R.id.repos_count);
        }


    }

}
