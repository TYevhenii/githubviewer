package com.examle.android.githubviewer;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zhenia on 27.12.2017.
 */

class JsonData extends AsyncTask<Void, Void, List<User>> implements RawData.DownloadCompletion {
    private static final String TAG = "JsonData";

    private List<User> mUserList = null;
    private String mBaseURL;

    private final DataAvailability mCallBack;
    private boolean runningOnSameThread = true;
    private String mSince;
    private Boolean mIsNeedUpdate;

    interface DataAvailability {
        void onDataAvailable(List<User> users, String baseUri, DownloadStatus status, Boolean isNeedUpdate);
    }

    public JsonData(DataAvailability callBack, String baseURL, String since) {
        Log.d(TAG, "JsonData: called");
        mSince = since;
        mBaseURL = baseURL;
        mCallBack = callBack;
        mIsNeedUpdate = Integer.valueOf(mSince) > 0;
    }


    @Override
    protected void onPostExecute(List<User> photos) {
        Log.d(TAG, "onPostExecute starts");

        if(mCallBack != null) {
            mCallBack.onDataAvailable(mUserList, mBaseURL, DownloadStatus.OK, mIsNeedUpdate);
        }

        Log.d(TAG, "onPostExecute ends");
    }

    @Override
    protected List<User> doInBackground(Void... params) {
        Log.d(TAG, "backgroundTask starts");
        String destinationUri = createUri(mSince);

        RawData rawData = new RawData(this);
        rawData.runInSameThread(destinationUri);
        Log.d(TAG, "backgroundTask ends");
        return mUserList;
    }

    private String createUri(String since) {
        Log.d(TAG, "createUri: starts");

        return Uri.parse(mBaseURL).buildUpon()
                .appendQueryParameter("since", since)
                .build().toString();
    }

    @Override
    public void onDownloadComplete(String data, DownloadStatus status) {
        Log.d(TAG, "onDownloadComplete starts. Status = " + status);

        if(status == DownloadStatus.OK) {
            mUserList = new ArrayList<>();

            try {
                JSONArray itemsArray = new JSONObject(data).getJSONArray("items");

                int length = itemsArray.length();
                for (int i = 0; i < length; i++) {
                    JSONObject jsonUser = itemsArray.getJSONObject(i);
                    String id = jsonUser.getString("id");
                    String name = jsonUser.getString("login");
                    String url = jsonUser.getString("avatar_url");
                    String usersUrl = jsonUser.getString("url");
                    String reposUrl = jsonUser.getString("repos_url");

                    User userObject = new User(Integer.parseInt(id), name, reposUrl, url);
                    //Github API cannot provide json for condition to show count of repositories for each users.
//                    ReposCount reposCount = new ReposCount(userObject);
//                    reposCount.execute(usersUrl);
                    mUserList.add(userObject);
                    Log.d(TAG, "onDownloadComplete " + userObject);
                }
            } catch(JSONException jsone) {
                jsone.printStackTrace();
                Log.e(TAG, "onDownloadComplete: Error processing Json data" + jsone.getMessage());
                status = DownloadStatus.FAILED_OR_EMPTY;
            }

        }
        Log.d(TAG, "onDownloadComplete end");
    }


}
