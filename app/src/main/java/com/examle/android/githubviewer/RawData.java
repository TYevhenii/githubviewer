package com.examle.android.githubviewer;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Zhenia on 28.12.2017.
 */

enum DownloadStatus { IDLE, PROCESSING, NOT_INITIALISED, FAILED_OR_EMPTY, OK}

class RawData {
    private static final String TAG = "RawData";

    private DownloadStatus mDownloadStatus;
    private final DownloadCompletion mCallback;

    interface DownloadCompletion {
        void onDownloadComplete(String data, DownloadStatus status);
    }


    public RawData(DownloadCompletion callback) {
        this.mDownloadStatus = DownloadStatus.IDLE;
        mCallback = callback;
    }

    void runInSameThread(String s) {
        Log.d(TAG, "runInSameThread starts");

//        onPostExecute(backgroundTask(s));
        if(mCallback != null) {
//            String resut = backgroundTask(s);
//            mCallback.onDownloadComplete(resut, mDownloadStatus);
            mCallback.onDownloadComplete(backgroundTask(s), mDownloadStatus);
        }

        Log.d(TAG, "runInSameThread ends");
    }

    protected String backgroundTask(String... strings) {
        HttpURLConnection connection = null;
//        BufferedReader  reader = null;

        if(strings == null) {
            mDownloadStatus = DownloadStatus.NOT_INITIALISED;
            return null;
        }

        try {
            URL url = new URL(strings[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET"); // HTTP request sent GET as default
            connection.connect();
            int response = connection.getResponseCode();
            Log.d(TAG, "backgroundTask: The response code was " + response);

            StringBuilder result = new StringBuilder();

            try(BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
//                String line = null;
//                while(null != (line = reader.readLine())) {
//                    result.append(line).append("\n");
//                }
                for(String line = reader.readLine(); line != null; line = reader.readLine()) {
                    result.append(line).append("\n");

                }
            }

            mDownloadStatus = DownloadStatus.OK;
            return result.toString();

        } catch (MalformedURLException ex) {
            Log.d(TAG, "backgroundTask: Invalid URL " + ex.getMessage());
        } catch(ProtocolException ex) {
            Log.d(TAG, "backgroundTask: Protocol exception: " + ex.getMessage());
        } catch(IOException ex) {
            Log.d(TAG, "backgroundTask: IOException reading data" + ex.getMessage());
        } catch( SecurityException ex) {
            Log.d(TAG, "backgroundTask: Security Exception. Needs permision? " + ex.getMessage());
        }
        finally {
            if(connection != null) {
                connection.disconnect();
                connection = null;
            }
        }
        mDownloadStatus = DownloadStatus.FAILED_OR_EMPTY;
        return null;
    }
}
