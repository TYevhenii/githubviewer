package com.examle.android.githubviewer;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class ReposCount extends AsyncTask<String, Void, String> {
    private static final String TAG = "ReposCount";

    private Result mResult;

    interface Result {
        public void onResultReposCount(String count);
    }

    public ReposCount(Result result) {
        mResult = result;
    }

    @Override
    protected String doInBackground(String... strings) {
        HttpURLConnection connection = null;
//        BufferedReader  reader = null;

        try {
            URL url = new URL(strings[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET"); // HTTP request sent GET as default
            connection.connect();
            int response = connection.getResponseCode();
            Log.d(TAG, "backgroundTask: The response code was " + response);

            StringBuilder result = new StringBuilder();

            try(BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
//                String line = null;
//                while(null != (line = reader.readLine())) {
//                    result.append(line).append("\n");
//                }
                for(String line = reader.readLine(); line != null; line = reader.readLine()) {
                    result.append(line).append("\n");

                }
            }

            String jsonData  = result.toString();
            String publicRepos = "0";
            try {
                JSONObject jsonPhoto = new JSONObject(jsonData);
                publicRepos = jsonPhoto.getString("public_repos");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return publicRepos;

        } catch (MalformedURLException ex) {
            Log.d(TAG, "backgroundTask: Invalid URL " + ex.getMessage());
        } catch(ProtocolException ex) {
            Log.d(TAG, "backgroundTask: Protocol exception: " + ex.getMessage());
        } catch(IOException ex) {
            Log.d(TAG, "backgroundTask: IOException reading data" + ex.getMessage());
        } catch( SecurityException ex) {
            Log.d(TAG, "backgroundTask: Security Exception. Needs permision? " + ex.getMessage());
        }
        finally {
            if(connection != null) {
                connection.disconnect();
                connection = null;
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(String publicRepos) {
        super.onPostExecute(publicRepos);
        mResult.onResultReposCount(publicRepos);
    }
}
