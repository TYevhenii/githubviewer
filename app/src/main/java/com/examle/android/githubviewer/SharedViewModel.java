package com.examle.android.githubviewer;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class SharedViewModel extends ViewModel {
    private final MutableLiveData<String> searched = new MutableLiveData<String>();

    private final MutableLiveData<UserSelect> selected = new MutableLiveData<UserSelect>();

    public void select(UserSelect url) {
        selected.setValue(url);
    }

    public LiveData<UserSelect> getSelected() {
        return selected;
    }

    public void search(String url) {
        searched.setValue(url);
    }

    public LiveData<String> getSearched() {
        return searched;
    }

}