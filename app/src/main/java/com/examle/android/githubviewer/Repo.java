package com.examle.android.githubviewer;

import java.io.Serializable;

class Repo implements Serializable {
    private static final long serialVersionUID = 1L;

    private int mId;
    private String mRepoName;
    private String mRepoDescription;

    public Repo(int id, String repoName, String repoDescription) {
        mId = id;
        mRepoName = repoName;
        mRepoDescription = repoDescription;
    }

    public int getId() {
        return mId;
    }

    public String getRepoName() {
        return mRepoName;
    }

    public String getRepoDescription() {
        return mRepoDescription;
    }

    @Override
    public String toString() {
        return "Repo{" +
                "mId=" + mId +
                ", mRepoName='" + mRepoName + '\'' +
                ", mRepoDescription='" + mRepoDescription + '\'' +
                '}';
    }
}
