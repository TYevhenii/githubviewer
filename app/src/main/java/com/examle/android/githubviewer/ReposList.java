package com.examle.android.githubviewer;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zhenia on 27.12.2017.
 */

class ReposList extends AsyncTask<Void, Void, List<Repo>> implements RawData.DownloadCompletion {
    private static final String TAG = "JsonData";

    private List<Repo> mReposList = null;
    private String mBaseURL;

    private final DataAvailability mCallBack;
    private boolean runningOnSameThread = true;
    private DownloadStatus mStatus = DownloadStatus.OK;

    interface DataAvailability {
        void onDataAvailable(List<Repo> users, String baseUri, DownloadStatus status);
    }

    public ReposList(DataAvailability callBack, String baseURL) {
        Log.d(TAG, "JsonData: called");
        mBaseURL = baseURL;
        mCallBack = callBack;
    }


    @Override
    protected void onPostExecute(List<Repo> photos) {
        Log.d(TAG, "onPostExecute starts");

        if(mCallBack != null) {
            mCallBack.onDataAvailable(mReposList, mBaseURL, mStatus);
        }

        Log.d(TAG, "onPostExecute ends");
    }

    @Override
    protected List<Repo> doInBackground(Void... params) {
        Log.d(TAG, "backgroundTask starts");
        String destinationUri = createUri();

        RawData rawData = new RawData(this);
        rawData.runInSameThread(destinationUri);
        Log.d(TAG, "backgroundTask ends");
        return mReposList;
    }

    private String createUri() {
        Log.d(TAG, "createUri: starts");
        return Uri.parse(mBaseURL).toString();
    }

    @Override
    public void onDownloadComplete(String data, DownloadStatus status) {
        Log.d(TAG, "onDownloadComplete starts. Status = " + status);

        if(status == DownloadStatus.OK) {
            mReposList = new ArrayList<>();

            try {
                JSONArray itemsArray = new JSONArray(data);

                int length = itemsArray.length();
                for (int i = 0; i < length; i++) {
                    JSONObject jsonUser = itemsArray.getJSONObject(i);
                    String id = jsonUser.getString("id");
                    String name = jsonUser.getString("name");
                    String description = jsonUser.getString("description");

                    Repo userObject = new Repo(Integer.parseInt(id), name, description);
                    mReposList.add(userObject);
                    Log.d(TAG, "onDownloadComplete " + userObject);
                }
            } catch(JSONException jsone) {
                jsone.printStackTrace();
                Log.e(TAG, "onDownloadComplete: Error processing Json data" + jsone.getMessage());
                mStatus = DownloadStatus.FAILED_OR_EMPTY;
            }
        }
        Log.d(TAG, "onDownloadComplete end");
    }


}
