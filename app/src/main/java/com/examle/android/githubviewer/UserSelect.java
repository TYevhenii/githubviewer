package com.examle.android.githubviewer;

class UserSelect {
    private String user;
    private String select;

    public UserSelect(String user, String select) {
        this.user = user;
        this.select = select;
    }

    public String getUser() {
        return user;
    }

    public String getSelect() {
        return select;
    }
}
