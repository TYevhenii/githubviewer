package com.examle.android.githubviewer;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.widget.SearchView;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private Toolbar mToolbar;
    private RepoListFragment mSecondListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        //Add back navigation in the title bar
//        setSupportActionBar(toolbar);

        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.container_search) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity layout
            SearchListFragment firstFragment = new SearchListFragment();

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container_search, firstFragment).commit();
            SharedViewModel model = ViewModelProviders.of(MainActivity.this).get(SharedViewModel.class);
            model.getSelected().observe(this, item -> {
                mSecondListFragment = new RepoListFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction
                        .replace(R.id.container_search, mSecondListFragment);
                fragmentTransaction.commit();
            });
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu2) {
        Log.d(TAG, "onCreateOptionsMenu: starts");
        mToolbar.inflateMenu(R.menu.menu_search);
        Menu menu = mToolbar.getMenu();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.app_bar_search).getActionView();
        SearchableInfo searchableInfo = searchManager.getSearchableInfo(getComponentName());
        searchView.setSearchableInfo(searchableInfo);

        searchView.setIconified(false);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.d(TAG, "onQueryTextSubmit: called");
                SharedViewModel model = ViewModelProviders.of(MainActivity.this).get(SharedViewModel.class);
                model.search("https://api.github.com/search/users?q=" + query + "+in%3Alogin&type=Users");
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchView.setOnCloseListener(() -> {
                    searchView.clearFocus();
                    return true;
                }
        );

        searchView.setIconified(false);

        Log.d(TAG, "onCreateOptionsMenu: returned " + true);
        return true;
    }
}
